import sortAlgorithms.insertionSort;
import hackerrank.birthDayChocolate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        String s1="Hello";
        String s2="World";

        Set<Character> lettersS1 = new HashSet<Character>();
        Set<Character> lettersS2 = new HashSet<Character>();

        for(int i=0; i<s1.length(); i++){
            lettersS1.add(s1.charAt(i));
        }

        for (int i=0; i<s2.length(); i++){
            lettersS2.add(s2.charAt(i));
        }

        lettersS1.retainAll(lettersS2);
        System.out.println( "S1 :"+lettersS1 + "S2"+ lettersS2  );
        if (lettersS1.size()>0){
            System.out.println("");
            System.out.println("YES");
        }else{
            System.out.println("NO");
        }








        int arr[] = {12, 11, 13, 5, 6};

        //insertionSort.sort(arr);
        //printArray(arr);
        List<Integer> s = new ArrayList<Integer>();

        s.add(1);
        s.add(2);
        s.add(1);
        s.add(3);
        s.add(2);

        birthDayChocolate.birthday(s, 3, 2);

    }

    static void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");

        System.out.println();
    }

    static void printArrayList( List <Integer> arr) {
        int n = arr.size();
        for (int i = 0; i < n; ++i)
            System.out.print(arr.get(i) + " ");

        System.out.println();
    }
}
