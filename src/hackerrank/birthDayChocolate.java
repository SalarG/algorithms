package hackerrank;

import java.util.List;

public class birthDayChocolate {


    public static int birthday(List<Integer> squares, int d, int m) {

        int ways = 0;
        int sum = 0;
        //Find if there is a way to break the chocolate at all
        if(m <= squares.size())
            for(int i = 0; i < m; i++)
                sum += squares.get(i);
        if(sum == d) ways++;
        ///////////////////////////////////////////////////////

        //Check other possible ways to break it by using a sliding window
        for(int i = 0; i < squares.size()-m; i++)
        {
            sum = sum - squares.get(i) + squares.get(i+m);
            if(sum == d) ways++;
        }
        System.out.println("ways: "+ways);
        return ways;


    }
}
